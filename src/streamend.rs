use log::error;
use std::io::Read;
use std::marker::PhantomData;
use std::os::unix::net::UnixStream;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use std::thread;

pub struct Monitor {
    stream_ended: Arc<AtomicBool>,
    _unsync: std::marker::PhantomData<std::cell::Cell<()>>,
    _unsend: std::marker::PhantomData<std::sync::MutexGuard<'static, ()>>,
}

impl Monitor {
    pub fn new(stream: &mut UnixStream) -> Monitor {
        let _ = stream.set_nonblocking(false);

        let stream_ended = Arc::new(AtomicBool::new(false));
        let notify_thread = thread::current();
        let thread_flag = stream_ended.clone();
        let mut thread_stream = stream.try_clone().unwrap();

        thread::spawn(move || {
            loop {
                let mut buf = [0u8; 64];
                match thread_stream.read(&mut buf) {
                    Err(e) if e.kind() == std::io::ErrorKind::Interrupted => {}
                    Err(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => break,
                    Err(e) => {
                        error!("Error reading from monitored stream: {}", e);
                        break;
                    }
                    Ok(n) if n == 0 => break,
                    Ok(_) => {}
                }
            }
            thread_flag.store(true, Ordering::Release);
            notify_thread.unpark();
        });

        Monitor {
            stream_ended,
            _unsync: PhantomData,
            _unsend: PhantomData,
        }
    }

    pub fn is_ended(&self) -> bool {
        self.stream_ended.load(Ordering::Acquire)
    }
}
