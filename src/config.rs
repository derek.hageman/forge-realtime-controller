use serde_derive::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub realtime: ConfigRealtime,
}

#[derive(Debug, Deserialize, Default)]
#[serde(default)]
pub struct ConfigRealtime {
    #[serde(default = "default_socket")]
    pub socket: String,

    #[serde(default = "default_storage")]
    pub storage: String,

    #[serde(default = "default_retain_seconds")]
    pub retain_seconds: f64,

    #[serde(default = "default_retain_count")]
    pub retain_count: usize,

    #[serde(default = "default_time_rounding")]
    pub time_rounding: f64,
}

fn default_socket() -> String {
    "/run/forge-vis-realtime.socket".to_string()
}

fn default_storage() -> String {
    "/var/lib/forge-vis-realtime".to_string()
}

fn default_retain_seconds() -> f64 {
    86400.0
}

fn default_retain_count() -> usize {
    4000
}

fn default_time_rounding() -> f64 {
    10.0
}

impl Config {
    pub fn load() -> Config {
        toml::from_str(
            &std::fs::read_to_string("settings.toml").expect("Unable to load configuration file"),
        )
        .unwrap()
    }
}
