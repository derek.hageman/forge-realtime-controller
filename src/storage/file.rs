use super::value::Value;
use std::collections::HashMap;
use std::io::{Read, Write};
use std::path::Path;
use std::time::SystemTime;

type Fields = HashMap<String, Vec<Value>>;
pub type Update = Vec<(String, Value)>;

pub struct File {
    times: Vec<u64>,
    data: Fields,
}

fn read_u32<R: Read>(f: &mut R) -> std::io::Result<u32> {
    let mut b = [0u8; 4];
    f.read_exact(&mut b)?;
    Ok(u32::from_le_bytes(b))
}

fn read_string<R: Read>(f: &mut R) -> std::io::Result<String> {
    let string_length = read_u32(f)? as usize;
    let mut raw_string = vec![0u8; string_length];
    f.read_exact(&mut raw_string[..])?;
    String::from_utf8(raw_string)
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))
}

fn write_string<W: Write>(f: &mut W, s: &String) -> std::io::Result<()> {
    let raw_string = s.as_bytes();
    f.write(&(raw_string.len() as u32).to_le_bytes())?;
    f.write(raw_string)?;
    Ok(())
}

impl File {
    pub fn new() -> File {
        File {
            times: Vec::new(),
            data: Fields::new(),
        }
    }

    pub fn read<P: AsRef<Path>>(filename: P) -> std::io::Result<File> {
        let mut f = std::fs::File::open(filename)?;

        let n_records = read_u32(&mut f)? as usize;
        let mut times: Vec<u64> = Vec::with_capacity(n_records);
        let mut raw_times = vec![0u8; n_records * 8];
        f.read_exact(&mut raw_times[..])?;
        for time_index in 0..n_records {
            let origin = time_index * 8;
            let element: &[u8; 8] = raw_times[origin..(origin + 8)].try_into().unwrap();
            times.push(u64::from_le_bytes(*element));
        }

        let n_fields = read_u32(&mut f)? as usize;
        let mut data: Fields = Fields::with_capacity(n_fields);
        for _ in 0..n_fields {
            let field_name = read_string(&mut f)?;

            let mut values: Vec<Value> = Vec::with_capacity(n_records);
            for _ in 0..n_records {
                values.push(Value::read(&mut f)?);
            }

            data.insert(field_name, values);
        }

        Ok(File { times, data })
    }

    pub fn save<P: AsRef<Path>>(&self, filename: P) -> std::io::Result<()> {
        // Explicit remove so the new file isn't connected to any open FDs
        let _ = std::fs::remove_file(&filename);
        let mut f = std::fs::File::create(filename)?;

        let n_records = self.times.len();
        f.write(&(u32::try_from(n_records).unwrap().to_le_bytes()))?;
        let mut raw_times = vec![0u8; n_records * 8];
        for time_index in 0..n_records {
            let origin = time_index * 8;
            raw_times[origin..(origin + 8)]
                .copy_from_slice(&(self.times[time_index].to_le_bytes()));
        }
        f.write(&raw_times[..])?;

        f.write(&(u32::try_from(self.data.len()).unwrap().to_le_bytes()))?;
        for (field_name, values) in &self.data {
            write_string(&mut f, field_name)?;

            for v in values {
                v.write(&mut f)?;
            }
        }

        Ok(())
    }

    pub fn update(&mut self, time: u64, merge_limit: u64, fields: Update) {
        if self.times.last().map_or(true, |t| {
            let diff = {
                if *t < time {
                    time - *t
                } else {
                    *t - time
                }
            };
            diff >= merge_limit
        }) {
            self.times.push(time);
            for values in self.data.values_mut() {
                values.push(Value::Missing);
            }
        }

        for (field_name, insert) in fields {
            if let Some(existing) = self.data.get_mut(&field_name) {
                let last = existing.len() - 1;
                existing[last] = insert;
            } else {
                let mut values = Vec::with_capacity(self.times.len());
                for _ in 0..(self.times.len() - 1) {
                    values.push(Value::Missing);
                }
                values.push(insert);
                self.data.insert(field_name, values);
            }
        }
    }

    pub fn is_empty(&self) -> bool {
        self.times.len() == 0
    }

    pub fn prune(&mut self, retain_ms: u64, retain_count: usize) -> bool {
        let epoch_ms = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_millis() as u64;
        let prune_before = epoch_ms - retain_ms;
        let mut remove_count = match self.times.binary_search(&prune_before) {
            Ok(i) => i,
            Err(i) => i,
        };

        if self.times.len() - remove_count > retain_count {
            remove_count = self.times.len() - retain_count;
        }

        if remove_count == 0 {
            return false;
        }

        self.times.drain(0..remove_count);
        for values in self.data.values_mut() {
            values.drain(0..remove_count);
        }

        true
    }
}

pub fn read_update_fields<R: Read>(source: &mut R) -> std::io::Result<Update> {
    let mut raw_count = [0u8; 4];
    source.read_exact(&mut raw_count)?;
    let n_fields = u32::from_le_bytes(raw_count) as usize;
    let mut fields = Update::with_capacity(n_fields);
    for _ in 0..n_fields {
        let field_name = read_string(source)?;
        let value = Value::read(source)?;
        fields.push((field_name, value));
    }

    Ok(fields)
}

pub fn serialize_update_fields(fields: &Update, epoch_ms: u64) -> Vec<u8> {
    let mut result = vec![0u8; 16];
    result[0..4].copy_from_slice(&(1u32.to_le_bytes()));
    result[4..12].copy_from_slice(&(epoch_ms.to_le_bytes()));
    result[12..16].copy_from_slice(&(u32::try_from(fields.len()).unwrap().to_le_bytes()));

    for (field_name, value) in fields {
        write_string(&mut result, field_name).unwrap();
        value.write(&mut result).unwrap();
    }

    result
}
