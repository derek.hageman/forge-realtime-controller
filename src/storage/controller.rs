use super::file::{serialize_update_fields, File, Update};
use crate::config::Config;
use crate::streamend;
use libsystemd::daemon::{self, NotifyState};
use log::{debug, error, trace};
use lru::LruCache;
use std::collections::HashMap;
use std::io::Write;
use std::os::unix::net::UnixStream;
use std::path::{Path, PathBuf};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    mpsc, Arc, Mutex,
};
use std::thread;
use std::time::{Duration, Instant, SystemTime};

const STORAGE_VERSION: u32 = 1;
const LOADED_CACHE_SIZE: usize = 16;

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct DataKey {
    station: String,
    data_name: String,
}

impl DataKey {
    pub fn new(station: String, data_name: String) -> DataKey {
        DataKey { station, data_name }
    }

    fn storage_file(&self, storage_directory: &PathBuf) -> PathBuf {
        let mut filename = String::from("realtime.");
        filename.push_str(&self.station);
        filename.push_str(".");
        filename.push_str(&self.data_name);
        storage_directory.join(filename)
    }
}

impl std::fmt::Display for DataKey {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} {}", &self.station, &self.data_name)
    }
}

type StreamChunk = Vec<u8>;

struct ConnectedDataStream {
    target: mpsc::SyncSender<StreamChunk>,
    thread: thread::Thread,
}

struct DataEntry {
    storage_file: PathBuf,
    stream_targets: Vec<ConnectedDataStream>,
}

impl DataEntry {
    pub fn new(storage_directory: &PathBuf, key: &DataKey) -> DataEntry {
        DataEntry {
            storage_file: key.storage_file(storage_directory),
            stream_targets: Vec::new(),
        }
    }

    fn broadcast_update(&mut self, fields: &Update, epoch_ms: u64) {
        if self.stream_targets.is_empty() {
            return;
        }

        trace!(
            "Sending stream update to {} targets",
            self.stream_targets.len()
        );

        let data = serialize_update_fields(fields, epoch_ms);
        self.stream_targets
            .retain_mut(|stream| match stream.target.try_send(data.clone()) {
                Ok(_) => {
                    stream.thread.unpark();
                    true
                }
                Err(e) => match e {
                    mpsc::TrySendError::Full(_) => true,
                    mpsc::TrySendError::Disconnected(_) => false,
                },
            });
    }

    fn can_evict(&self) -> bool {
        self.stream_targets.len() == 0
    }

    fn get_source(&self) -> Option<std::fs::File> {
        match std::fs::File::open(&self.storage_file) {
            Ok(file) => Some(file),
            Err(e) if e.kind() == std::io::ErrorKind::NotFound => None,
            Err(e) => {
                error!("Error opening data: {}", e);
                None
            }
        }
    }
}

struct LoadedData {
    contents: File,
    storage_file: PathBuf,
    dirty: bool,
}

impl LoadedData {
    pub fn new(storage_directory: &PathBuf, key: &DataKey) -> LoadedData {
        LoadedData {
            contents: File::new(),
            storage_file: key.storage_file(storage_directory),
            dirty: false,
        }
    }

    pub fn load(storage_directory: &PathBuf, key: &DataKey) -> std::io::Result<LoadedData> {
        let storage_file = key.storage_file(storage_directory);
        let contents = File::read(&storage_file)?;

        Ok(LoadedData {
            contents,
            storage_file,
            dirty: false,
        })
    }

    fn save(&mut self) -> std::io::Result<bool> {
        if self.contents.is_empty() {
            trace!(
                "Removing empty storage file {}",
                self.storage_file.display()
            );
            return match std::fs::remove_file(&self.storage_file) {
                Ok(_) => {
                    self.dirty = false;
                    Ok(false)
                }
                Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
                    self.dirty = false;
                    Ok(false)
                }
                Err(e) => Err(e),
            };
        }
        if !self.dirty {
            return Ok(true);
        }
        trace!("Writing data to {}", self.storage_file.display());
        self.contents.save(&self.storage_file)?;
        self.dirty = false;
        Ok(true)
    }

    fn update(&mut self, fields: Update, epoch_ms: u64, merge_ms: u64) {
        self.dirty = true;
        self.contents.update(epoch_ms, merge_ms, fields);
    }

    fn prune(&mut self, retain_ms: u64, retain_count: usize) {
        if self.contents.prune(retain_ms, retain_count) {
            self.dirty = true;
        }
    }
}

struct WriteOperation {
    key: DataKey,
    fields: Update,
}

struct ActiveRead {
    storage: Option<std::fs::File>,
}

struct ReadOperation {
    key: DataKey,
    active: mpsc::SyncSender<ActiveRead>,
}

struct ActiveStream {
    storage: Option<std::fs::File>,
    chunk: mpsc::Receiver<StreamChunk>,
}

struct StreamOperation {
    key: DataKey,
    active: mpsc::SyncSender<ActiveStream>,
    thread: thread::Thread,
}

enum DataOperation {
    Write(WriteOperation),
    Read(ReadOperation),
    Stream(StreamOperation),
}

enum PruneState {
    Inactive,
    Active(Vec<DataKey>),
}

struct Data {
    entries: HashMap<DataKey, DataEntry>,
    loaded: LruCache<DataKey, LoadedData>,

    operation_issue: mpsc::SyncSender<DataOperation>,
    operations: mpsc::Receiver<DataOperation>,

    storage_directory: PathBuf,
    rounding_ms: u64,

    retain_ms: u64,
    retain_count: usize,

    prune_advance: Instant,
    prune_state: PruneState,
}

fn check_storage_version(storage_directory: &PathBuf) -> bool {
    let version_contents = match std::fs::read_to_string(&storage_directory.join(".version")) {
        Ok(s) => s,
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => return false,
        Err(e) => panic!("Error reading storage version: {}", e),
    };
    let version_number: u32 = match version_contents.parse() {
        Ok(i) => i,
        Err(_) => return false,
    };
    if version_number != STORAGE_VERSION {
        debug!("Realtime storage version does not match");
        return false;
    }
    true
}

fn remove_storage_file<P: AsRef<Path>>(file_name: P) {
    match std::fs::remove_file(file_name) {
        Ok(_) => (),
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => (),
        Err(e) => panic!("Error removing realtime entry: {}", e),
    }
}

fn initialize_storage_directory(storage_directory: &PathBuf) {
    debug!("Initializing realtime storage");

    for file in match std::fs::read_dir(storage_directory) {
        Ok(i) => i,
        Err(e) => panic!(
            "Unable to read storage directory {}: {}",
            storage_directory.display(),
            e
        ),
    } {
        let path = file.unwrap().path();
        if path.is_file() {
            remove_storage_file(&path);
        }
    }

    std::fs::write(
        &storage_directory.join(".version"),
        STORAGE_VERSION.to_string().as_bytes(),
    )
    .unwrap();
}

fn save_pruned(
    entries: &mut HashMap<DataKey, DataEntry>,
    data_key: &DataKey,
    loaded_data: &mut LoadedData,
) {
    match loaded_data.save() {
        Ok(have_contents) => {
            if !have_contents {
                if let Some(data_entry) = entries.get(&data_key) {
                    if data_entry.can_evict() {
                        entries.remove(&data_key);
                    }
                }
            }
        }
        Err(e) => {
            error!("Error saving pruned data: {}", e);
        }
    }
}

impl Data {
    pub fn new(config: &Config) -> Data {
        let mut entries: HashMap<DataKey, DataEntry> = HashMap::new();
        let loaded: LruCache<DataKey, LoadedData> = LruCache::unbounded();

        let mut rounding_ms = (config.realtime.time_rounding * 1000.0).round() as u64;
        if rounding_ms < 1 {
            rounding_ms = 1;
        }

        let mut retain_ms = (config.realtime.retain_seconds * 1000.0).round() as u64;
        if retain_ms < 1 {
            retain_ms = 1;
        }
        let retain_count = config.realtime.retain_count;

        let storage_directory = PathBuf::from(&config.realtime.storage);

        let (operation_issue, operations) = mpsc::sync_channel(10);

        if !check_storage_version(&storage_directory) {
            initialize_storage_directory(&storage_directory);
        } else {
            for file in std::fs::read_dir(&storage_directory).unwrap() {
                let path = file.unwrap().path();
                let metadata = match path.metadata() {
                    Ok(m) => m,
                    Err(_) => continue,
                };
                if !metadata.is_file() {
                    continue;
                }
                let file_name = match path.file_name() {
                    None => continue,
                    Some(s) => match s.to_str() {
                        None => continue,
                        Some(s) => s,
                    },
                };
                let mut parts = file_name.split(".");
                match parts.next() {
                    None => continue,
                    Some(code) => {
                        if code != "realtime" {
                            continue;
                        }
                    }
                }

                if metadata.len() == 0 {
                    remove_storage_file(&path);
                    continue;
                }

                let key = match (parts.next(), parts.next()) {
                    (None, _) => {
                        remove_storage_file(&path);
                        continue;
                    }
                    (_, None) => {
                        remove_storage_file(&path);
                        continue;
                    }
                    (Some(station), Some(data_name)) => {
                        DataKey::new(station.to_string(), data_name.to_string())
                    }
                };

                let mut loaded_data = match LoadedData::load(&storage_directory, &key) {
                    Ok(d) => d,
                    Err(e) => {
                        debug!("Removing un-loadable {}: {}", file_name, e);
                        continue;
                    }
                };

                loaded_data.prune(retain_ms, retain_count);

                match loaded_data.save() {
                    Ok(have_contents) => {
                        if !have_contents {
                            continue;
                        }
                    }
                    Err(e) => {
                        error!("Error saving initial data: {}", e);
                        continue;
                    }
                }

                let data_entry = DataEntry::new(&storage_directory, &key);
                entries.insert(key, data_entry);
            }

            debug!("Loaded {} realtime records", entries.len());
        }

        Data {
            entries,
            loaded,
            operation_issue,
            operations,
            storage_directory,
            rounding_ms,
            retain_ms,
            retain_count,
            prune_advance: Instant::now() + Duration::from_secs(15 * 60),
            prune_state: PruneState::Inactive,
        }
    }

    fn evict_cache(&mut self) {
        while self.loaded.len() > LOADED_CACHE_SIZE {
            match self.loaded.pop_lru() {
                None => break,
                Some((data_key, original_data)) => {
                    let mut loaded_data = original_data;
                    loaded_data.prune(self.retain_ms, self.retain_count);
                    match loaded_data.save() {
                        Ok(have_contents) => {
                            if !have_contents {
                                if self.entries.get(&data_key).unwrap().can_evict() {
                                    trace!("Removing unused data entry for {}", &data_key);
                                    self.entries.remove(&data_key);
                                }
                            }
                        }
                        Err(e) => {
                            error!("Error saving evicted data: {}", e);
                        }
                    }
                }
            }
        }
    }

    fn update(&mut self, key: DataKey, fields: Update) {
        let epoch_ms = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_millis() as u64;
        let rounded_ms = (epoch_ms / self.rounding_ms) * self.rounding_ms;

        let data_entry = match self.entries.get_mut(&key) {
            None => {
                trace!("Inserting new entry to write {}", &key);

                self.entries
                    .insert(key.clone(), DataEntry::new(&self.storage_directory, &key));

                let mut loaded = LoadedData::new(&self.storage_directory, &key);
                loaded.update(fields, rounded_ms, self.rounding_ms);
                self.loaded.put(key, loaded);

                self.evict_cache();
                return;
            }
            Some(data_entry) => data_entry,
        };

        data_entry.broadcast_update(&fields, epoch_ms);

        if let Some(loaded_data) = self.loaded.get_mut(&key) {
            trace!("Using cached data for update to {}", &key);
            loaded_data.update(fields, rounded_ms, self.rounding_ms);
        } else {
            let mut loaded_data = match LoadedData::load(&self.storage_directory, &key) {
                Ok(loaded) => {
                    trace!("Loaded existing data for update to {}", &key);
                    loaded
                }
                Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
                    trace!("Creating  empty record for update to {}", &key);
                    LoadedData::new(&self.storage_directory, &key)
                }
                Err(e) => {
                    error!("Unable to load data file for update: {}", e);
                    LoadedData::new(&self.storage_directory, &key)
                }
            };
            loaded_data.update(fields, rounded_ms, self.rounding_ms);
            self.loaded.put(key, loaded_data);
        }

        self.evict_cache();
    }

    fn acquire_send_file(&mut self, key: &DataKey) -> Option<std::fs::File> {
        if let Some(loaded_data) = self.loaded.peek_mut(key) {
            if let Err(e) = loaded_data.save() {
                error!("Error saving data for send: {}", e);
                return None;
            }
        }

        match self.entries.get(key) {
            Some(data_entry) => data_entry.get_source(),
            None => None,
        }
    }

    fn read(&mut self, key: DataKey) -> ActiveRead {
        ActiveRead {
            storage: self.acquire_send_file(&key),
        }
    }

    fn stream(&mut self, key: DataKey, notify_thread: thread::Thread) -> ActiveStream {
        let storage = self.acquire_send_file(&key);
        let (chunk_input, chunk) = mpsc::sync_channel(8);

        match self.entries.get_mut(&key) {
            None => {
                let mut data_entry = DataEntry {
                    storage_file: key.storage_file(&self.storage_directory),
                    stream_targets: Vec::with_capacity(1),
                };
                data_entry.stream_targets.push(ConnectedDataStream {
                    target: chunk_input,
                    thread: notify_thread,
                });
                self.entries.insert(key, data_entry);
            }
            Some(data_entry) => {
                data_entry.stream_targets.push(ConnectedDataStream {
                    target: chunk_input,
                    thread: notify_thread,
                });
            }
        };

        ActiveStream { storage, chunk }
    }

    fn work(&mut self) -> Duration {
        loop {
            match self.operations.try_recv() {
                Ok(operation) => match operation {
                    DataOperation::Write(write) => self.update(write.key, write.fields),
                    DataOperation::Read(read) => {
                        let _ = read.active.send(self.read(read.key));
                    }
                    DataOperation::Stream(stream) => {
                        let _ = stream.active.send(self.stream(stream.key, stream.thread));
                    }
                },
                Err(_) => break,
            }
        }

        let now = Instant::now();
        if self.prune_advance + Duration::from_millis(5) <= now {
            if matches!(self.prune_state, PruneState::Inactive) {
                let mut keys = Vec::with_capacity(self.entries.len());
                for data_key in self.entries.keys() {
                    keys.push(data_key.clone());
                }
                debug!("Starting data prune for {} records", keys.len());
                self.prune_state = PruneState::Active(keys);
            }
            if let PruneState::Active(keys) = &mut self.prune_state {
                while let Some(data_key) = keys.pop() {
                    trace!("Pruning {}", &data_key);
                    if let Some(loaded_data) = self.loaded.peek_mut(&data_key) {
                        loaded_data.prune(self.retain_ms, self.retain_count);
                        save_pruned(&mut self.entries, &data_key, loaded_data);
                    } else {
                        let mut loaded_data =
                            match LoadedData::load(&self.storage_directory, &data_key) {
                                Ok(loaded) => loaded,
                                Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
                                    if let Some(data_entry) = self.entries.get(&data_key) {
                                        if data_entry.can_evict() {
                                            trace!("Removing empty entry for {}", &data_key);
                                            self.entries.remove(&data_key);
                                        }
                                    }
                                    continue;
                                }
                                Err(e) => {
                                    error!("Unable to load data file for pruning: {}", e);
                                    if let Some(data_entry) = self.entries.get(&data_key) {
                                        if data_entry.can_evict() {
                                            trace!("Removing errored entry for {}", &data_key);
                                            self.entries.remove(&data_key);
                                        }
                                    }
                                    continue;
                                }
                            };

                        loaded_data.prune(self.retain_ms, self.retain_count);
                        save_pruned(&mut self.entries, &data_key, &mut loaded_data);
                    }

                    break;
                }

                if keys.is_empty() {
                    self.prune_state = PruneState::Inactive;
                    self.prune_advance = now + Duration::from_secs(15 * 60);
                } else {
                    self.prune_advance = now + Duration::from_millis(50);
                }
            }
        }

        self.prune_advance - now
    }

    fn shutdown(&mut self) {
        for (_, loaded_data) in &mut self.loaded {
            loaded_data.prune(self.retain_ms, self.retain_count);
            if let Err(e) = loaded_data.save() {
                error!("Error saving data: {}", e);
            }
        }
        self.loaded.clear();
    }
}

pub struct Controller {
    data: Arc<Mutex<Data>>,
    shutdown_flag: Arc<AtomicBool>,
    thread: thread::JoinHandle<()>,
}

pub struct ServerInterface {
    data: Arc<Mutex<Data>>,
    thread: thread::Thread,
}

pub struct ConnectionInterface {
    issue: mpsc::SyncSender<DataOperation>,
    thread: thread::Thread,
}

fn run(shutdown_flag: Arc<AtomicBool>, data: Arc<Mutex<Data>>) {
    let _ = daemon::notify(false, &[NotifyState::Ready]);

    let mut next_watchdog = Instant::now() + Duration::from_secs(10);

    while !shutdown_flag.load(Ordering::Acquire) {
        let data_sleep = {
            let mut data = data.lock().unwrap();
            data.work()
        };

        let now = Instant::now();
        if now + Duration::from_millis(10) >= next_watchdog {
            let _ = daemon::notify(false, &[NotifyState::Watchdog]);
            next_watchdog = now + Duration::from_secs(10);
        }
        let watchdog_sleep = next_watchdog - now;

        thread::park_timeout(std::cmp::min(data_sleep, watchdog_sleep));
    }
}

impl Controller {
    pub fn new(config: &Config) -> Controller {
        let data = Arc::new(Mutex::new(Data::new(config)));
        let shutdown_flag = Arc::new(AtomicBool::new(false));

        let th_data = data.clone();
        let th_shutdown_flag = shutdown_flag.clone();
        let thread = thread::spawn(move || run(th_shutdown_flag, th_data));

        Controller {
            data,
            shutdown_flag,
            thread,
        }
    }

    pub fn attach_interface(&mut self) -> ServerInterface {
        ServerInterface {
            data: self.data.clone(),
            thread: self.thread.thread().clone(),
        }
    }

    pub fn shutdown(self) {
        let _ = daemon::notify(false, &[NotifyState::Stopping]);

        self.shutdown_flag.store(true, Ordering::Release);
        self.thread.thread().unpark();

        self.thread.join().unwrap();
        let mut data = self.data.lock().unwrap();
        data.shutdown();
    }
}

impl ServerInterface {
    pub fn connection(&mut self) -> ConnectionInterface {
        let issue = {
            let data = self.data.lock().unwrap();
            data.operation_issue.clone()
        };
        ConnectionInterface {
            issue,
            thread: self.thread.clone(),
        }
    }
}

fn send_storage_file(
    source: Option<std::fs::File>,
    target: &mut UnixStream,
) -> std::io::Result<()> {
    let mut source_file = match source {
        None => return Ok(()),
        Some(f) => f,
    };
    trace!("Sending existing data file");
    std::io::copy(&mut source_file, target)?;
    Ok(())
}

impl ConnectionInterface {
    pub fn write(
        &mut self,
        station: String,
        data_name: String,
        fields: Update,
    ) -> std::io::Result<()> {
        self.issue
            .send(DataOperation::Write(WriteOperation {
                key: DataKey::new(station, data_name),
                fields,
            }))
            .unwrap();
        self.thread.unpark();

        Ok(())
    }

    pub fn read(
        &mut self,
        station: String,
        data_name: String,
        target: &mut UnixStream,
    ) -> std::io::Result<()> {
        let (active_response, active) = mpsc::sync_channel(1);
        self.issue
            .send(DataOperation::Read(ReadOperation {
                key: DataKey::new(station, data_name),
                active: active_response,
            }))
            .unwrap();
        self.thread.unpark();
        let active = active
            .recv()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;

        send_storage_file(active.storage, target)?;

        Ok(())
    }

    pub fn stream(
        &mut self,
        station: String,
        data_name: String,
        target: &mut UnixStream,
    ) -> std::io::Result<()> {
        let (active_response, active) = mpsc::sync_channel(1);
        self.issue
            .send(DataOperation::Stream(StreamOperation {
                key: DataKey::new(station, data_name),
                active: active_response,
                thread: thread::current(),
            }))
            .unwrap();
        self.thread.unpark();
        let active = active
            .recv()
            .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))?;

        send_storage_file(active.storage, target)?;
        trace!("Starting data stream");

        let monitor = streamend::Monitor::new(target);
        let chunks = active.chunk;
        loop {
            if monitor.is_ended() {
                trace!("Stream disconnected");
                break;
            }
            match chunks.try_recv() {
                Err(e) if e == mpsc::TryRecvError::Empty => {}
                Err(_) => {
                    trace!("Controller chunk stream ended");
                    break;
                }
                Ok(chunk) => {
                    trace!("Sending data stream chunk");
                    match target.write_all(&chunk[..]) {
                        Ok(_) => {}
                        Err(e) if e.kind() == std::io::ErrorKind::BrokenPipe => break,
                        Err(e) => return Err(e),
                    }
                    target.flush()?;
                }
            }
            thread::park();
        }

        Ok(())
    }
}
