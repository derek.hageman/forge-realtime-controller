use std::io::{Read, Write};

pub enum Value {
    Missing,
    Float([u8; 4]),
    ArrayOfFloat(Vec<u8>),
}

const VALUE_TYPE_MISSING: u8 = 0;
const VALUE_TYPE_FLOAT: u8 = 1;
const VALUE_TYPE_ARRAY_OF_FLOAT: u8 = 2;

impl Value {
    pub fn read<R: Read>(f: &mut R) -> std::io::Result<Value> {
        let mut value_type = [0u8; 1];
        f.read_exact(&mut value_type)?;

        match value_type[0] {
            VALUE_TYPE_MISSING => Ok(Value::Missing),

            VALUE_TYPE_FLOAT => {
                let mut raw = [0u8; 4];
                f.read_exact(&mut raw)?;
                Ok(Value::Float(raw))
            }

            VALUE_TYPE_ARRAY_OF_FLOAT => {
                let mut raw_count = [0u8; 4];
                f.read_exact(&mut raw_count)?;
                let count = u32::from_le_bytes(raw_count) as usize;
                let mut raw = vec![0u8; count * 4];
                f.read_exact(&mut raw[..])?;
                Ok(Value::ArrayOfFloat(raw))
            }

            _ => Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                "invalid value type",
            )),
        }
    }

    pub fn write<W: Write>(&self, f: &mut W) -> std::io::Result<()> {
        match self {
            Value::Missing => {
                f.write(&[VALUE_TYPE_MISSING])?;
            }

            Value::Float(value) => {
                f.write(&[VALUE_TYPE_FLOAT])?;
                f.write(value)?;
            }

            Value::ArrayOfFloat(values) => {
                f.write(&[VALUE_TYPE_ARRAY_OF_FLOAT])?;
                let count = values.len() / 4;
                f.write(&(u32::try_from(count).unwrap().to_le_bytes()))?;
                f.write(&values[..])?;
            }
        }
        Ok(())
    }
}
