use libsystemd::activation::{self, IsType};
use log::{info, warn};
use std::os::unix::net::UnixListener;
use std::sync::atomic::AtomicBool;
use std::sync::Arc;

mod config;
mod server;
mod storage;
mod streamend;

fn main() {
    env_logger::init();

    let multiple_terminate = Arc::new(AtomicBool::new(false));
    for sig in signal_hook::consts::TERM_SIGNALS {
        signal_hook::flag::register_conditional_shutdown(*sig, 1, Arc::clone(&multiple_terminate))
            .unwrap();
        signal_hook::flag::register(*sig, Arc::clone(&multiple_terminate)).unwrap();
    }
    let mut signals =
        signal_hook::iterator::Signals::new(signal_hook::consts::TERM_SIGNALS).unwrap();

    let config = config::Config::load();

    let sockets: Vec<UnixListener> = match activation::receive_descriptors(true).ok() {
        None => {
            std::fs::remove_file(&config.realtime.socket).ok();
            info!("Listening on {}", &config.realtime.socket);
            vec![UnixListener::bind(&config.realtime.socket).unwrap()]
        }
        Some(fds) => {
            let mut r: Vec<UnixListener> = Vec::new();

            for fd in fds {
                if !fd.is_unix() {
                    warn!("Unsupported socket type in systemd activation");
                    continue;
                }

                r.push(unsafe {
                    use std::os::unix::io::{FromRawFd, IntoRawFd};
                    UnixListener::from_raw_fd(fd.into_raw_fd())
                });
            }

            if r.len() == 0 {
                panic!("No viable socket activation found (no unix sockets)");
            }
            info!("Listening on {} systemd activated sockets", r.len());

            r
        }
    };

    let mut controller = storage::controller::Controller::new(&config);

    for socket in sockets {
        server::launch(socket, &mut controller);
    }

    loop {
        if signals.wait().count() != 0 {
            break;
        }
    }

    info!("Shutting down");
    controller.shutdown();
}
