use crate::storage::controller::{ConnectionInterface, Controller};
use crate::storage::file::read_update_fields;
use log::{debug, trace, warn};
use std::io::Read;
use std::os::unix::net::{UnixListener, UnixStream};

const CONNECTION_TYPE_WRITE: u8 = 0;
const CONNECTION_TYPE_STREAM: u8 = 1;
const CONNECTION_TYPE_READ: u8 = 2;

fn connection_string_arg(stream: &mut UnixStream) -> std::io::Result<String> {
    let mut raw_length = [0u8; 4];
    stream.read_exact(&mut raw_length)?;
    let string_length = u32::from_le_bytes(raw_length) as usize;
    let mut raw_string = vec![0u8; string_length];
    stream.read_exact(&mut raw_string[..])?;
    String::from_utf8(raw_string)
        .map_err(|e| std::io::Error::new(std::io::ErrorKind::InvalidData, e))
}

fn station_and_data_name(stream: &mut UnixStream) -> std::io::Result<(String, String)> {
    let station = connection_string_arg(stream)?;
    let data_name = connection_string_arg(stream)?;
    Ok((station, data_name))
}

fn connection(mut stream: UnixStream, mut interface: ConnectionInterface) -> std::io::Result<()> {
    let _ = stream.set_nonblocking(false);
    let mut connection_type = [0u8; 1];
    stream.read_exact(&mut connection_type)?;

    match connection_type[0] {
        CONNECTION_TYPE_WRITE => {
            debug!("Accepted write connection");
            loop {
                match station_and_data_name(&mut stream) {
                    Ok((station, data_name)) => {
                        let fields = read_update_fields(&mut stream)?;
                        trace!(
                            "Writing {} fields to {} {}",
                            fields.len(),
                            &station,
                            &data_name
                        );
                        interface.write(station, data_name, fields)?;
                    }
                    Err(e) if e.kind() == std::io::ErrorKind::UnexpectedEof => {
                        break;
                    }
                    Err(e) => {
                        return Err(e);
                    }
                }
            }
        }
        CONNECTION_TYPE_STREAM => {
            let (station, data_name) = station_and_data_name(&mut stream)?;
            debug!("Accepted stream connection for {} {}", station, data_name);
            interface.stream(station, data_name, &mut stream)?;
        }
        CONNECTION_TYPE_READ => {
            let (station, data_name) = station_and_data_name(&mut stream)?;
            debug!("Accepted read connection for {} {}", station, data_name);
            interface.read(station, data_name, &mut stream)?;
        }
        _ => {
            warn!("Invalid connection type {}", connection_type[0]);
        }
    }

    trace!("Connection closed");

    let _ = stream.shutdown(std::net::Shutdown::Both);

    Ok(())
}

pub fn launch(listener: UnixListener, controller: &mut Controller) {
    let mut interface = controller.attach_interface();

    std::thread::spawn(move || {
        for stream in listener.incoming() {
            match stream {
                Ok(stream) => {
                    let ci = interface.connection();
                    std::thread::spawn(|| match connection(stream, ci) {
                        Ok(_) => {}
                        Err(err) => {
                            warn!("Error in connection: {}", err);
                        }
                    });
                }
                Err(_) => {}
            }
        }
    });
}
